package middleware

import (
	"team/config"
	"team/common/web"
)

func MustInstalled(next web.Handler) web.Handler {
	return func(c *web.Context) {
		if config.Installed {
			next(c)
		} else {
			c.JSON(200, web.Map{"err": "系统尚未初始化"})
		}
	}
}