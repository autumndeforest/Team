package config

import (
	"fmt"
	"log"

	"team/common/auth"
	"team/common/ini"
	"team/common/orm"
)

var Installed = false

var ExtraAuth auth.Provider = nil

type AppInfo struct {
	Name string
	Port int
}

var App = &AppInfo{
	Name: "Team",
	Port: 8080,
}

func (a *AppInfo) Addr() string {
	return fmt.Sprintf(":%d", a.Port)
}

type MySQLInfo struct {
	Host     string
	User     string
	Password string
	Database string
}

var MySQL = &MySQLInfo{
	Host:     "127.0.0.1:3306",
	User:     "root",
	Password: "root",
	Database: "team",
}

// 获取Mysql的url
func (m *MySQLInfo) URL() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s)/%s?multiStatements=true&charset=utf8&collation=utf8_general_ci",
		m.User, m.Password, m.Host, m.Database)
}

// 加载程序配置
func Load() {
	setting, err := ini.Load("./team.ini")
	if err != nil {
		log.Fatalf("Failed to parse configuration file: team.ini. Reason: %v\n", err)
	}

	Read(setting)

	// 链接数据库
	if err = orm.OpenDB("mysql", MySQL.URL()); err != nil {
		log.Fatalf("Failed to connect to MySQL database: %s. Reason: %v", MySQL.URL(), err)
	}

	log.Printf("Service will started at :%d\n", App.Port)
	Installed = true
}

// 读取配置文件获取信息
func Read(setting *ini.Ini) {
	defer func() {
		if except := recover(); except != nil {
			log.Fatalf("Parse ./team.ini failed. Reason: %v", except)
		}
	}()

	App.Name = setting.GetString("app", "name")
	App.Port = setting.GetInt("app", "port")

	MySQL.Host = setting.GetString("mysql", "host")
	MySQL.User = setting.GetString("mysql", "user")
	MySQL.Password = setting.GetString("mysql", "password")
	MySQL.Database = setting.GetString("mysql", "database")
}