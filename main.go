package main

import (
	"strings"

	"team/common/web"
	"team/config"
	"team/controller"
	"team/middleware"

	rice "github.com/GeertJohan/go.rice"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// 加载配置
	config.Load()

	resBox := rice.MustFindBox("view/dist")
	mainPage := strings.ReplaceAll(resBox.MustString("app.html"), "__APP_NAME__", config.App.Name)

	// 路由创建
	router := web.NewRouter()
	router.Use(middleware.Logger)
	router.Use(middleware.PanicAsError)

	router.SetPage("/", mainPage)
	router.StaticFS("/assets", resBox.HTTPBox())
	router.StaticFS("/uploads", web.Dir("uploads"))

	router.GET("/home", controller.Home)

	router.GET("/logout", controller.Logout, middleware.MustInstalled)
	router.POST("/login", controller.Login, middleware.MustInstalled)

	api := router.Group("/api")
	api.Use(middleware.MustInstalled)
	api.Use(middleware.AutoLogin)
	api.Use(middleware.MustLogined)
	api.UseController("/user", new(controller.User))
	api.UseController("/task", new(controller.Task))
	api.UseController("/project", new(controller.Project))
	api.UseController("/document", new(controller.Document))
	api.UseController("/file", new(controller.File))
	api.UseController("/notice", new(controller.Notice))

	router.UseController(
		"/admin",
		new(controller.Admin),
		middleware.MustInstalled,
		middleware.AutoLogin,
		middleware.MustLoginedAsAdmin)

	router.Start(config.App.Addr())
}
