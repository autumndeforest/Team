package web

import (
	"errors"
	"strconv"
)

type Value struct {
	data []string
}

var errMissing = errors.New("Parameter NOT exists")

func (v *Value) Bool() (bool, error) {
	if v.data == nil || len(v.data) == 0 {
		return false, nil
	}

	return strconv.ParseBool(v.data[0])
}

func (v *Value) MustBool(errMsg string) bool {
	b, e := v.Bool()
	Assert(e == nil, errMsg)
	return b
}

func (v *Value) Int() (int64, error) {
	if v.data == nil || len(v.data) == 0 {
		return 0, errMissing
	}

	return strconv.ParseInt(v.data[0], 10, 64)
}

func (v *Value) MustInt(errMsg string) int64 {
	n, e := v.Int()
	Assert(e == nil, errMsg)
	return n
}

func (v *Value) Ints() ([]int, error) {
	if v.data == nil {
		return []int{}, errMissing
	}

	ret := []int{}
	for _, s := range v.data {
		n, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			return ret, err
		}

		ret = append(ret, int(n))
	}

	return ret, nil
}

func (v *Value) MustInts(errMsg string) []int {
	n, e := v.Ints()
	Assert(e == nil, errMsg)
	return n
}

func (v *Value) Uint() (uint64, error) {
	if v.data == nil || len(v.data) == 0 {
		return 0, errMissing
	}

	return strconv.ParseUint(v.data[0], 10, 32)
}

func (v *Value) MustUint(errMsg string) uint64 {
	u, e := v.Uint()
	Assert(e == nil, errMsg)
	return u
}

func (v *Value) Uints() ([]uint64, error) {
	if v.data == nil {
		return nil, errMissing
	}

	ret := []uint64{}
	for _, s := range v.data {
		n, err := strconv.ParseUint(s, 10, 64)
		if err != nil {
			return nil, err
		}

		ret = append(ret, n)
	}

	return ret, nil
}

func (v *Value) MustUints(errMsg string) []uint64 {
	u, e := v.Uints()
	Assert(e == nil, errMsg)
	return u
}

func (v *Value) Float() (float64, error) {
	if v.data == nil || len(v.data) == 0 {
		return 0, errMissing
	}

	return strconv.ParseFloat(v.data[0], 32)
}

func (v *Value) MustFloat(errMsg string) float64 {
	f, e := v.Float()
	Assert(e == nil, errMsg)
	return f
}

func (v *Value) Floats() ([]float64, error) {
	if v.data == nil {
		return nil, errMissing
	}

	ret := []float64{}
	for _, s := range v.data {
		n, err := strconv.ParseFloat(s, 64)
		if err != nil {
			return nil, err
		}

		ret = append(ret, n)
	}

	return ret, nil
}

func (v *Value) MustFloats(errMsg string) []float64 {
	f, e := v.Floats()
	Assert(e == nil, errMsg)
	return f
}

func (v *Value) String() string {
	if v.data == nil || len(v.data) == 0 {
		return ""
	}

	return v.data[0]
}

func (v *Value) MustString(errMsg string) string {
	if v.data == nil || len(v.data) == 0 || len(v.data[0]) == 0 {
		panic(errors.New(errMsg))
	}

	return v.data[0]
}

func (v *Value) Strings() []string {
	return v.data
}

func (v *Value) MustStrings(errMsg string) []string {
	if v.data == nil {
		panic(errors.New(errMsg))
	}

	return v.data
}
