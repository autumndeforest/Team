package web

import (
	"bufio"
	"net"
	"net/http"
)

type (
	Responser struct {
		writer     http.ResponseWriter
		statusCode int
		statusDone bool
	}
)

func (r *Responser) Status() int {
	return r.statusCode
}

func (r *Responser) Header() http.Header {
	return r.writer.Header()
}

func (r *Responser) Write(data []byte) (int, error) {
	return r.writer.Write(data)
}

func (r *Responser) WriteHeader(code int) {
	if r.statusDone {
		return
	}

	r.statusCode = code
	r.statusDone = true
	r.writer.WriteHeader(code)
}

func (r *Responser) Flush() {
	r.writer.(http.Flusher).Flush()
}

func (r *Responser) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return r.writer.(http.Hijacker).Hijack()
}
