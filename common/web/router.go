package web

import (
	"errors"
	"net/http"
	"regexp"
	"strings"
)

type (
	Handler func(c *Context)

	Middleware func(next Handler) Handler

	Controller interface {
		Register(group *Router)
	}

	Dispatcher struct {
		pattern *regexp.Regexp
		router  *Router
		names   []string
		handler Handler
	}

	Router struct {
		prefix      string
		notFound    Handler
		middlewares []Middleware
		children    map[string]*Router
		dispatchers map[string][]*Dispatcher
	}
)

func NewRouter() *Router {
	return &Router{
		prefix:      "",
		middlewares: []Middleware{},
		children:    make(map[string]*Router),
		dispatchers: make(map[string][]*Dispatcher),
		notFound:    WrapFunc(http.NotFound),
	}
}

// handler格式转化
func Wrap(handler http.Handler) Handler {
	return func(c *Context) {
		handler.ServeHTTP(c.rsp, c.req)
	}
}

func Dir(path string) http.FileSystem {
	return http.Dir(path)
}

// 处理函数格式转化
func WrapFunc(handle func(w http.ResponseWriter, r *http.Request)) Handler {
	return func(c *Context) {
		handle(c.rsp, c.req)
	}
}

func AssertError(err error) {
	if err != nil {
		panic(err)
	}
}

func Assert(expr bool, errMsg string) {
	if !expr {
		panic(errors.New(errMsg))
	}
}

func (r *Router) Start(addr string) error {
	return http.ListenAndServe(addr, r)
}

func (r *Router) ServeHTTP(rsp http.ResponseWriter, req *http.Request) {
	c := &Context{
		Session:     nil,
		req:         req,
		rsp:         &Responser{writer: rsp, statusCode: 200, statusDone: false},
		routeParams: make(map[string]string),
		queryParams: nil,
	}

	dispachers, ok := r.dispatchers[req.Method]
	if !ok {
		r.Invoke(c, r.notFound)
		return
	}

	for _, test := range dispachers {
		matches := test.pattern.FindSubmatch([]byte(req.URL.Path))
		if matches != nil && string(matches[0]) == req.URL.Path {
			params := matches[1:]
			for idx, param := range params {
				c.routeParams[test.names[idx]] = string(param)
			}

			test.router.Invoke(c, test.handler)
			return
		}
	}

	r.Invoke(c, r.notFound)
}

func (r *Router) Invoke(c *Context, handler Handler) {
	caller := handler

	for i := len(r.middlewares) - 1; i >= 0; i-- {
		caller = r.middlewares[i](caller)
	}

	c.Session = Sessions.Start(c)
	caller(c)
}

func (r *Router) SetNotFound(handler Handler) {
	r.notFound = handler
}

func (r *Router) SetPage(url string, html string) {
	r.GET(url, func(c *Context) {
		c.HTML(200, html)
	})
}

func (r *Router) Add(method string, pattern string, handler Handler, middlewares ...Middleware) {
	pattern = r.prefix + pattern

	names := []string{}
	paths := strings.Split(pattern, "/")
	pattern = ""

	for _, path := range paths {
		if path == "" {
			continue
		}

		size := len(path)
		start := path[0]
		end := path[size-1]

		if start == ':' {
			name := path[1:]
			names = append(names, name)
			if name == "id" {
				pattern = pattern + `/([\d]+)`
			} else {
				pattern = pattern + `/([\w]+)`
			}
		} else if start == '{' && end == '}' {
			idx := strings.Index(path, ":")
			names = append(names, path[1:idx])
			pattern = pattern + "/(" + path[idx+1:size-1] + ")"
		} else {
			pattern = pattern + "/" + path
		}
	}

	if len(pattern) == 0 {
		pattern = "/"
	}

	caller := handler
	if middlewares != nil && len(middlewares) > 0 {
		for i := len(middlewares) - 1; i >= 0; i-- {
			caller = middlewares[i](caller)
		}
	}

	dispatcher := &Dispatcher{
		pattern: regexp.MustCompile(pattern),
		router:  r,
		names:   names,
		handler: caller,
	}

	_, ok := r.dispatchers[method]
	if ok {
		r.dispatchers[method] = append(r.dispatchers[method], dispatcher)
	} else {
		r.dispatchers[method] = []*Dispatcher{dispatcher}
	}
}

func (r *Router) StaticFS(prefix string, fs http.FileSystem, middlewares ...Middleware) {
	uri := strings.TrimRight(prefix, "/") + "/"
	r.Add("GET", uri+`[\s\S]+`, Wrap(http.StripPrefix(uri, http.FileServer(fs))), middlewares...)
}

func (r *Router) GET(pattern string, handler Handler, middlewares ...Middleware) {
	r.Add("GET", pattern, handler, middlewares...)
}

func (r *Router) POST(pattern string, handler Handler, middlewares ...Middleware) {
	r.Add("POST", pattern, handler, middlewares...)
}

func (r *Router) PUT(pattern string, handler Handler, middlewares ...Middleware) {
	r.Add("PUT", pattern, handler, middlewares...)
}

func (r *Router) PATCH(pattern string, handler Handler, middlewares ...Middleware) {
	r.Add("PATCH", pattern, handler, middlewares...)
}

func (r *Router) DELETE(pattern string, handler Handler, middlewares ...Middleware) {
	r.Add("DELETE", pattern, handler, middlewares...)
}

func (r *Router) Group(prefix string) *Router {
	group, ok := r.children[prefix]
	if ok {
		return group
	}

	group = &Router{
		prefix:      r.prefix + prefix,
		notFound:    r.notFound,
		middlewares: append([]Middleware{}, r.middlewares...),
		children:    make(map[string]*Router),
		dispatchers: r.dispatchers,
	}

	r.children[prefix] = group
	return group
}

// 添加中间件
func (r *Router) Use(middleware Middleware) {
	r.middlewares = append(r.middlewares, middleware)
	for _, child := range r.children {
		child.middlewares = append(child.middlewares, middleware)
	}
}

func (r *Router) UseController(prefix string, controller Controller, middlewares ...Middleware) {
	group := r.Group(prefix)

	if middlewares != nil && len(middlewares) > 0 {
		group.middlewares = append(group.middlewares, middlewares...)
	}

	controller.Register(group)
}
