package web

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type (
	Map map[string]interface{}

	Context struct {
		Session     *Session

		req         *http.Request
		rsp         *Responser
		routeParams map[string]string
		queryParams url.Values
	}
)

// 请求方法
func (c *Context) Method() string {
	return c.req.Method
}

// URL信息
func (c *Context) URL() *url.URL {
	return c.req.URL
}

// 获取远程客户端IP
func (c *Context) RemoteIP() string {
	if ip := c.RequestHeader().Get("X-Forwarded-For"); ip != "" {
		return strings.Split(ip, ", ")[0]
	}

	if ip := c.RequestHeader().Get("X-Real-IP"); ip != "" {
		return ip
	}

	ip, _, _ := net.SplitHostPort(c.req.RemoteAddr)
	return ip
}

// 获取
func (c *Context) RemoteAddr() string {
	return c.req.RemoteAddr
}

func (c *Context) Body() io.ReadCloser {
	return c.req.Body
}

func (c *Context) BodyAsJSON(to interface{}) error {
	buf, err := ioutil.ReadAll(c.req.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(buf, to)
}

func (c *Context) RequestHeader() http.Header {
	return c.req.Header
}

func (c *Context) ResponseHeader() http.Header {
	return c.rsp.Header()
}

func (c *Context) EndSession() {
	cookies := c.req.Cookies()
	for _, cookie := range cookies {
		c.SetCookie(&http.Cookie{
			Name:   cookie.Name,
			Value:  "",
			Path:   cookie.Path,
			MaxAge: -1,
		})
	}

	Sessions.Lock()
	defer Sessions.Unlock()
	delete(Sessions.all, c.Session.id)
}

func (c *Context) Cookie(key string) (*http.Cookie, error) {
	return c.req.Cookie(key)
}

func (c *Context) SetCookie(cookie *http.Cookie) {
	http.SetCookie(c.rsp, cookie)
}

func (c *Context) Status() int {
	return c.rsp.Status()
}

func (c *Context) SetStatus(code int) {
	c.rsp.WriteHeader(code)
}

func (c *Context) RouteValue(name string) *Value {
	v, ok := c.routeParams[name]
	if ok {
		return &Value{data: []string{v}}
	}

	return &Value{}
}

func (c *Context) QueryValue(name string) *Value {
	if c.queryParams == nil {
		c.queryParams = c.req.URL.Query()
	}

	v, ok := c.queryParams[name]
	if ok {
		return &Value{data: v}
	}

	return &Value{}
}

func (c *Context) FormValue(name string) *Value {
	if c.req.Form == nil {
		c.req.ParseMultipartForm(64 << 20)
	}

	v, ok := c.req.Form[name]
	if ok {
		return &Value{data: v}
	}

	return &Value{}
}

func (c *Context) PostFormValue(name string) *Value {
	if c.req.PostForm == nil {
		c.req.ParseMultipartForm(64 << 20)
	}

	v, ok := c.req.PostForm[name]
	if ok {
		return &Value{data: v}
	}

	return &Value{}
}

func (c *Context) MultipartForm() *multipart.Form {
	if c.req.MultipartForm == nil {
		c.req.ParseMultipartForm(64 << 20)
	}

	return c.req.MultipartForm
}

func (c *Context) Redirect(status int, url string) {
	http.Redirect(c.rsp, c.req, url, status)
}

func (c *Context) Stream(status int, contentType string, reader io.Reader) error {
	c.rsp.WriteHeader(status)
	c.rsp.Header().Set("Content-Type", contentType)

	_, err := io.Copy(c.rsp, reader)
	return err
}

func (c *Context) Flush() {
	c.rsp.Flush()
}

func (c *Context) Blob(status int, contentType string, blob []byte) error {
	c.rsp.WriteHeader(status)
	c.rsp.Header().Set("Content-Type", contentType)

	_, err := c.rsp.Write(blob)
	return err
}

func (c *Context) String(status int, str string) error {
	return c.Blob(status, "text/plain", []byte(str))
}

func (c *Context) HTML(status int, html string) error {
	return c.Blob(status, "text/html", []byte(html))
}

func (c *Context) JSON(status int, v interface{}) error {
	blob, err := json.Marshal(v)
	if err != nil {
		return err
	}

	return c.Blob(status, "application/json", blob)
}

func (c *Context) File(status int, path string) error {
	return c.FileWithName(status, path, "")
}

func (c *Context) FileWithName(status int, path, name string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}

	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		return err
	}

	if info.IsDir() {
		return errors.New(path + " is NOT a file but directory")
	}

	if len(name) > 0 {
		c.rsp.Header().Set("Content-Disposition", "attachement;filename="+name)
	}

	http.ServeContent(c.rsp, c.req, info.Name(), info.ModTime(), file)
	return nil
}
