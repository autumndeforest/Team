package auth

type Provider interface {
	Verify(account, password string) error
}
